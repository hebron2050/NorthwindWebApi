﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NorthwindWebApi.Models
{
    public class Role
    {
        private int RoleId { get; set; }
        private string RoleName { get; set; }
        private string Description { get; set; }
        public bool DeActive { get; set; }
        public bool Active { get; set; }  
        public long UserID {get;set;}
        public string GroupRole { get; set; }
        public string ActionPriority { get; set; }

    }
}