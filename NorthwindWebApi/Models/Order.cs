﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NorthwindWebApi.Models
{
    public class Order
    {
        private int OrderId { get; set; }
        private string Name { get; set; }
        private string Description { get; set; }
        private double Unit { get; set; }
        private double UnitPrice { get; set; }
        private double Amount { get; set; }
        private bool Delivery { get; set; }
        private bool Approved { get; set; }
        private bool Cancel { get; set; }



    }
}