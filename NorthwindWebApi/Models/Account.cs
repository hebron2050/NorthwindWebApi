﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NorthwindWebApi.Models
{
    public class Account
    {
        private string AccountName { get; set; }
        private string BankName { get; set; }
        private string AccountNumber { get; set; }
        private string TransactionNumber { get; set;}
        private string BranchofBank { get; set; }

    }
}