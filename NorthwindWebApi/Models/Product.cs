﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NorthwindWebApi.Models
{
    public class Product
    {
        private string Name { get; set; }
        private string Description { get; set; }
        private bool Active { get; set; }
        private bool Deleted { get; set; }

    }
}